/**
 * @file pose_selector.h
 * @brief Allows hot switching of mocap and vislam.
 *        Registers vislam and vicon frames.
 * @author Parker Lusk <parkerclusk@gmail.com>
 * @date 8 July 2019
 */

#pragma once

#include <algorithm>
#include <cctype>
#include <iostream>
#include <fstream>
#include <memory>
#include <string>
#include <sstream>
#include <vector>

#include <ros/ros.h>

#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2_ros/static_transform_broadcaster.h>

#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>

#include <std_srvs/Trigger.h>
#include <std_srvs/SetBool.h>

#include "vislam_utils/tf2_helpers.h"
#include "vislam_utils/utils.h"

namespace acl {
namespace vislam_utils {

  class PoseSelector
  {
  public:
    PoseSelector(const ros::NodeHandle nh);
    ~PoseSelector() = default;
    
  private:
    ros::NodeHandle nh_;
    ros::Timer tim_tf_broadcast_;
    ros::ServiceServer srv_sample_, srv_select_;
    ros::Subscriber sub_mocap_, sub_vislam_; ///< poses to choose from
    ros::Publisher pub_pose_; ///< output pose
    ros::Publisher pub_vislam_registered_; ///< vislam body w.r.t mocap frame
    std::unique_ptr<tf2_ros::StaticTransformBroadcaster> tf_br_;

    std::string vehname_; ///< name of this vehicle (e.g., 'HX01')

    geometry_msgs::TransformStamped T_MO_; ///< vislam odom frame w.r.t mocap frame

    tf2::Transform T_IB_; ///< body (autopilot) w.r.t imu

    bool use_vislam_ = false; ///< true selects mocap pose
    bool shutdown_mocap_; ///< unsub from mocap after swap

    bool capture_ = false; ///< should we be filling a buffer with transforms?

    int nrCaptures_ = 0; ///< num captures that have been performed
    int nrSamples_; ///< num transform samples to use each capture
    std::vector<tf2::Transform> buf_T_MB_, buf_T_OB_; ///< transform buffers

    // Callbacks to receive mocap and VISLAM data
    void mocapCb(const geometry_msgs::PoseStampedPtr& msg);
    void vislamCb(const geometry_msgs::PoseStampedPtr& msg);

    void timerCb(const ros::TimerEvent& e);

    /**
     * @brief      Resgisters the VISLAM odom and mocap frames (i.e., T_MO)
     */
    void calibrate();

    /**
     * @brief      Handle ROS service request to sample current mocap poses.
     *             Also runs the calibration routine.
     *
     *
     * @param      req   request content (empty)
     * @param      res   response (true)
     *
     * @return     True if response handled
     */
    bool sampleSrvCb(std_srvs::Trigger::Request& req, std_srvs::Trigger::Response& res);

    /**
     * @brief      Handle ROS service request to select a pose stream.
     *
     *
     * @param      req   request content (true for vislam, false for mocap)
     * @param      res   response (true)
     *
     * @return     True if response handled
     */
    bool selectSrvCb(std_srvs::SetBool::Request& req, std_srvs::SetBool::Response& res);

  };

} // ns vislam_utils
} // ns acl
