function [imu, mocap, vislam, state, vicon, cmds] = readACLBag(veh, bagpath)
%READBAG Extracts messages into matrices without using custom msg defn

t = @(topic) ['/' veh '/' topic];

bag = rosbag(bagpath);

% Get IMU messages
bagsel = select(bag, 'Topic', t('imu'));
msgs = readMessages(bagsel,'DataFormat','struct'); imuMsg = [msgs{:}];
tsec = header([imuMsg.Header]);
tmp = [imuMsg.Gyro]; gX = [tmp.X]; gY = [tmp.Y]; gZ = [tmp.Z];
gyro = [gX' gY' gZ']';
tmp = [imuMsg.Accel]; aX = [tmp.X]; aY = [tmp.Y]; aZ = [tmp.Z];
accel = [aX' aY' aZ']';
imu = struct('t',tsec,'gyro',gyro,'accel',accel);

% Get mocap ground truth messages -- PoseStamped
bagsel = select(bag, 'Topic', t('world'));
msgs = readMessages(bagsel,'DataFormat','struct');
poseStampedMsg = [msgs{:}];
tsec = header([poseStampedMsg.Header]);
poseMsg = [poseStampedMsg.Pose];
[pos, quat] = PoseMessage(poseMsg);
mocap = struct('t',tsec,'position',pos,'quaternion',quat);

% Get vislam messages -- PoseStamped
bagsel = select(bag, 'Topic', t('pose_selector/vislam_registered'));
msgs = readMessages(bagsel,'DataFormat','struct');
poseStampedMsg = [msgs{:}];
tsec = header([poseStampedMsg.Header]);
poseMsg = [poseStampedMsg.Pose];
[pos, quat] = PoseMessage(poseMsg);
vislam = struct('t',tsec,'position',pos,'quaternion',quat);

% Get state messages
bagsel = select(bag, 'Topic', t('state'));
msgs = readMessages(bagsel,'DataFormat','struct'); stateMsg = [msgs{:}];
tsec = header([stateMsg.Header]); tstate = statestamp(stateMsg);
pos = Vector3Message([stateMsg.Pos]);
vel = Vector3Message([stateMsg.Vel]);
quat = QuaternionMessage([stateMsg.Quat]);
omega = Vector3Message([stateMsg.W]);
state = struct('t',tsec,'tstate',tstate,'pos',pos,'vel',vel,...
               'quat',quat,'omega',omega);
           
% Get VICON messages (with twist)
bagsel = select(bag, 'Topic', t('vicon'));
msgs = readMessages(bagsel,'DataFormat','struct'); viconMsg = [msgs{:}];
tsec = header([viconMsg.Header]);
[vel, ~] = TwistMessage([viconMsg.Twist]);
vicon = struct('t',tsec,'vel',vel);

% Get QuadCntrl messages
bagsel = select(bag, 'Topic', t('cmds'));
msgs = readMessages(bagsel,'DataFormat','struct'); cmdsMsg = [msgs{:}];
tsec = header([cmdsMsg.Header]);
[pos, ~] = PoseMessage([cmdsMsg.PoseActual]);
[posd, ~] = PoseMessage([cmdsMsg.Pose]);
[vel, ~] = TwistMessage([cmdsMsg.TwistActual]);
[veld, ~] = TwistMessage([cmdsMsg.Twist]);
rpy = Vector3Message([cmdsMsg.RpyActual]);
rpyd = Vector3Message([cmdsMsg.Rpy]);
cmds = struct('t',tsec,'pos',pos,'posd',posd,'vel',vel,'veld',veld,'rpy',rpy,'rpyd',rpyd);

% time sync
mint = min([min(mocap.t), min(vislam.t), min(imu.t), min(state.t), min(vicon.t), min(cmds.t)]);
mocap.t = mocap.t - mint;
vislam.t = vislam.t - mint;
imu.t = imu.t - mint;
state.t = state.t - mint;
vicon.t = vicon.t - mint;
cmds.t = cmds.t - mint;

end

function t = header(headerMsg)
stamp = [headerMsg.Stamp];
sec = [stamp.Sec];
nsec = [stamp.Nsec];
t = double(sec) + double(nsec)*1e-9;
end

function t = statestamp(stateMsg)
stamp = [stateMsg.StateStamp];
sec = [stamp.Sec];
nsec = [stamp.Nsec];
t = double(sec) + double(nsec)*1e-9;
end

function [pos, quat] = PoseMessage(poseMsg)
pos = Vector3Message([poseMsg.Position]);
quat = QuaternionMessage([poseMsg.Orientation]);
end

function [linvel, angvel] = TwistMessage(twistMsg)
linvel = Vector3Message([twistMsg.Linear]);
angvel = Vector3Message([twistMsg.Angular]);
end

function vec = Vector3Message(vectorMsg)
X = [vectorMsg.X]; Y = [vectorMsg.Y]; Z = [vectorMsg.Z];
vec = [X' Y' Z']';
end

function q = QuaternionMessage(quatMsg)
W = [quatMsg.W]; X = [quatMsg.X]; Y = [quatMsg.Y]; Z = [quatMsg.Z];
q = [W' X' Y' Z']';
end