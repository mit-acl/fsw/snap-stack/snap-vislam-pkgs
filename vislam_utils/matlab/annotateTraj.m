function annotateTraj(fignum)
%ANNOTATETRAJ Summary of this function goes here
%   Detailed explanation goes here

% get figure handle
fig = figure(fignum);

% turn off zoom (can't use KeyPressFcn in zoom mode)
zoom off;

% get axes handle
axes = findall(fig, 'type', 'axes');
ax = axes(1); % assume there is only one

% global vars to communicate with event handlers
global KEY_IS_PRESSED; KEY_IS_PRESSED = 0;
global NEXT_ANNOTATION; NEXT_ANNOTATION = 1;

% Let clicks work on the axis. See:
% https://www.mathworks.com/matlabcentral/answers/94935-why-is-the-buttondownfcn-callback-for-my-axes-not-activated-when-i-click-on-an-object-in-the-axes#answer_104287
hittest = get(ax, 'HitTest');
set(ax, 'HitTest', 'off');

% attach event listeners
set(fig, 'ButtonDownFcn', @MouseClickCb);
set(fig, 'KeyPressFcn', @KeyPressCb);

disp('Click on the plot to annotate.');
disp('Press any key to finish annotation.');
while ~KEY_IS_PRESSED
   drawnow;
end


% remove event listeners
set(fig, 'KeyPressFcn', []);
set(fig, 'ButtonDownFcn', []);

% restore HitTest
set(ax, 'HitTest', hittest);

end

function MouseClickCb(src, event)
global NEXT_ANNOTATION;

pt = get(gca,'CurrentPoint');
fprintf('Clicked: %.2f %.2f\n', pt(1,1),pt(1,2));
text(pt(1,1), pt(1,2), num2str(NEXT_ANNOTATION), 'FontSize',12);

NEXT_ANNOTATION = NEXT_ANNOTATION + 1;
end

function KeyPressCb(src, event)
global KEY_IS_PRESSED;
KEY_IS_PRESSED = 1;
end