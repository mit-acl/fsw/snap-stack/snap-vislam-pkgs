%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% VISLAM plotting from rosbag
%
% Parker Lusk
% 9 July 2019
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear, clc;
moclinw = 1;
estlinw = 1.5;

% bagfile = '~/Documents/bags/vio-tests/selector/selector_2019-07-09-14-23-26.bag';
% bagfile = '~/Documents/bags/vio-tests/selector/selector_2019-07-09-14-45-17.bag';
% bagfile = '~/Documents/bags/vio-tests/selector/selector_2019-07-09-14-55-37.bag';
% bagfile = '~/Documents/bags/vio-tests/selector/selector_floor_2019-07-09-15-05-26.bag';
bagfile = '~/Documents/bags/vio-tests/posesel/standreg_2019-07-20-15-49-10.bag';
% bagfile = '~/Documents/bags/vio-tests/posesel/standreg_2019-07-20-16-15-59.bag';
% bagfile = '~/Documents/bags/vio-tests/posesel/standreg_2019-07-20-16-28-16.bag';
[imu, mocap, vislam, state, vicon, cmds] = readACLBag('HX02', bagfile);

% time sync (when vislam is using monotonic clock instead of ros time)
offset = mocap.t(end) - vislam.t(end);
imu.t = imu.t - offset;
if imu.t(1)<0, imu.t = imu.t - imu.t(1); end
mocap.t = mocap.t - offset;
if mocap.t(1)<0, mocap.t = mocap.t - mocap.t(1); end
state.t = state.t - offset;
if state.t(1)<0, state.t = state.t - state.t(1); end
vicon.t = vicon.t - offset;
if vicon.t(1)<0, vicon.t = vicon.t - vicon.t(1); end
cmds.t = cmds.t - offset;
if cmds.t(1)<0, cmds.t = cmds.t - cmds.t(1); end

%%

% timing
ts = 0;
te = 110;
% imu
iNs = find(imu.t>=ts,1);
iNe = find(imu.t>=te,1);
if isempty(iNe), iNe = length(imu.t); end
% mocap
mNs = find(mocap.t>=ts,1);
mNe = find(mocap.t>=te,1);
if isempty(mNe), mNe = length(mocap.t); end
% vislam
vNs = find(vislam.t>=ts,1);
vNe = find(vislam.t>=te,1);
if isempty(vNe), vNe = length(vislam.t); end
% state
sNs = find(state.t>=ts,1);
sNe = find(state.t>=te,1);
if isempty(sNe), sNe = length(state.t); end
% vicon (vel)
vvNs = find(vicon.t>=ts,1);
vvNe = find(vicon.t>=te,1);
if isempty(vvNe), vvNe = length(vicon.t); end
% cmds (goal vs actual)
cNs = find(cmds.t>=ts,1);
cNe = find(cmds.t>=te,1);
if isempty(cNe), cNe = length(cmds.t); end

% IMU data
figure(1), clf;
subplot(211); hold on;
title('Raw IMU Data');
plot(imu.t(iNs:iNe), imu.gyro(:,iNs:iNe));
legend('X','Y','Z');
grid on; ylabel('Gyro [rad/s]');
subplot(212);
plot(imu.t(iNs:iNe), imu.accel(:,iNs:iNe));
grid on; ylabel('Accel [m/s/s]'); xlabel('Time [s]');

% Plot trajectory
figure(2), clf;
hold on; grid on;
plot(mocap.position(1,mNs:mNe), mocap.position(2,mNs:mNe),'LineWidth',1);
plot(vislam.position(1,vNs:vNe), vislam.position(2,vNs:vNe),'LineWidth',1);
legend('VICON','VISLAM'); title('X-Y Trajectory');
minmx = min(mocap.position(1,:)); maxmx = max(mocap.position(1,:));
minmy = min(mocap.position(2,:)); maxmy = max(mocap.position(2,:));
minvx = min(vislam.position(1,:)); maxvx = max(vislam.position(1,:));
minvy = min(vislam.position(2,:)); maxvy = max(vislam.position(2,:));
xaxis([min(minmx,minvx) max(maxmx,maxvx)]);
yaxis([min(minmy,minvy) max(maxmy,maxvy)]);
xlabel('X [m]'); ylabel('Y [m]'); axis equal

% XYZ traces
figure(3), clf;
for i = 1:3
    subplot(3,1,i); hold on; grid on;
    plot(mocap.t(mNs:mNe), mocap.position(i,mNs:mNe), 'k--','LineWidth',moclinw);
    plot(vislam.t(vNs:vNe), vislam.position(i,vNs:vNe),'LineWidth',estlinw);
    xlabel('Time [s]'); ylabel([char(87+i) ' [m]']);
end
subplot(311); legend('VICON','VISLAM'); title('Position');

% Velocity traces
figure(4), clf;
for i = 1:3
    subplot(3,1,i); hold on; grid on;
%     plot(vicon.t(vvNs:vvNe), vicon.vel(i,vvNs:vvNe), 'k:','LineWidth',0.1);
    plot(state.t(sNs:sNe), state.vel(i,sNs:sNe),'LineWidth',estlinw);
    xlabel('Time [s]'); ylabel(['v' char(87+i) ' [m/s]']);
end
subplot(311); legend('Snap Filtered'); title('Velocity');

% Quaternion traces
figure(5), clf;
for i = 1:4
    subplot(4,1,i); hold on; grid on;
    plot(mocap.t(mNs:mNe), mocap.quaternion(i,mNs:mNe), 'k--','LineWidth',moclinw);
    plot(vislam.t(vNs:vNe), vislam.quaternion(i,vNs:vNe),'LineWidth',estlinw);
    xlabel('Time [s]'); ylabel(['q' char(118+i)]);
end
subplot(411); legend('VICON','VISLAM'); title('Attitude');

% RPY traces
seq = 'XYZ';
mocapRPY = quat2eul(mocap.quaternion(:,mNs:mNe)', seq)*180/pi;
vislamRPY = quat2eul(vislam.quaternion(:,vNs:vNe)', seq)*180/pi;
AttLabel = {'R' 'P' 'Y'};
figure(6), clf;
for i = 1:3   
    subplot(3,1,i); hold on; grid on;
    plot(mocap.t(mNs:mNe), mocapRPY(:,i), 'k--','LineWidth',moclinw);
    plot(vislam.t(vNs:vNe), vislamRPY(:,i),'LineWidth',estlinw);
    xlabel('Time [s]'); ylabel(AttLabel{i});
end
subplot(311); legend('VICON','VISLAM'); title('Attitude RPY');

% Stats
mPos = timeseries(mocap.position(:,mNs:mNe), mocap.t(mNs:mNe));
vPos = timeseries(vislam.position(:,vNs:vNe), vislam.t(vNs:vNe));
[mPos, vPos] = synchronize(mPos, vPos, 'Union');
posErr = mPos.Data(:,:) - vPos.Data(:,:);
fprintf('Position Error: mean %.3f  min %.3f  max %.3f  std %.3f\n',...
        mean(vecnorm(posErr,2)), min(vecnorm(posErr,2)),...
        max(vecnorm(posErr,2)), std(vecnorm(posErr,2)));

vel = vicon.vel(:,sNs:sNe);
fprintf('Velocity: mean %.3f  min %.3f  max %.3f  std %.3f\n',...
        mean(vecnorm(vel,2)), min(vecnorm(vel,2)),...
        max(vecnorm(vel,2)), std(vecnorm(vel,2)));
    
%%

% Position goal traces
figure(7), clf;
for i = 1:3
    subplot(3,1,i); hold on; grid on;
    plot(cmds.t(cNs:cNe), cmds.posd(i,cNs:cNe), 'k--','LineWidth',moclinw);
    plot(cmds.t(cNs:cNe), cmds.pos(i,cNs:cNe),'LineWidth',estlinw);
    xlabel('Time [s]'); ylabel([char(87+i) ' [m]']);
end
subplot(311); legend('Desired','Actual'); title('Position');

% Velocity goal traces
figure(8), clf;
for i = 1:3
    subplot(3,1,i); hold on; grid on;
    plot(cmds.t(cNs:cNe), cmds.veld(i,cNs:cNe), 'k--','LineWidth',moclinw);
    plot(cmds.t(cNs:cNe), cmds.vel(i,cNs:cNe),'LineWidth',estlinw);
    xlabel('Time [s]'); ylabel(['v' char(87+i) ' [m/s]']);
end
subplot(311); legend('Desired','Actual'); title('Velocity');

% RPY goal traces
figure(9), clf;
AttLabel = {'R' 'P' 'Y'};
for i = 1:3
    subplot(3,1,i); hold on; grid on;
    plot(cmds.t(cNs:cNe), cmds.rpyd(i,cNs:cNe)*180/pi, 'k--','LineWidth',moclinw);
    plot(cmds.t(cNs:cNe), cmds.rpy(i,cNs:cNe)*180/pi,'LineWidth',estlinw);
    xlabel('Time [s]'); ylabel(AttLabel{i});
end
subplot(311); legend('Desired','Actual'); title('RPY');