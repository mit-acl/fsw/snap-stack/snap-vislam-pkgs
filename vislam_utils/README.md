VISLAM Utilities
================

The `pose_selector` node is responsible for (1) registering the local vio frame with mocap, (2) acting as a mux between mocap and vio inputs (i.e., the output `pose_selector/pose` topic should be subscribed to by `snap` and the switch from mocap to vio will be seemless).

- Use `rosservice call sample /HX01/pose_selector/sample "{}"` to register vio with mocap (hint use tab-tab).
- Use `rosservice call select /HX01/pose_selector/select "data: true"` to switch to vio, use *false* to switch to mocap (hint use tab-tab).

## Connections

Double check that the pose selector mux is connected as expected. Compare with the following `rqt_graph`:

![](https://gitlab.com/mit-acl/fsw/snap-stack/snap-vislam-pkgs/-/wikis/assets/pose_selector_rqt_graph.png)