/**
 * @file utils.cpp
 * @brief Utility methods associated with mocap fusion
 * @author Parker Lusk <parkerclusk@gmail.com>
 * @date 5 March 2019
 */

#include "vislam_utils/utils.h"

namespace acl {
namespace vislam_utils {

bool Utils::parse_xyzYPR(const std::string& xyzYPR, tf2::Transform& T)
{
  constexpr int expectedValues = 6; // x y z Y P R
  constexpr char delim = ' ';

  std::vector<double> values;
  std::stringstream ss(xyzYPR);

  // attempt to parse the string
  try {
    std::string tmp;
    while (std::getline(ss, tmp, delim)) {
      values.push_back(std::stod(tmp));
    }
  } catch (...) {
    return false;
  }

  if (values.size() != expectedValues) {
    return false;
  }

  // create transform
  tf2::Vector3 p(values[0], values[1], values[2]);
  tf2::Quaternion q;
  q.setRPY(values[5], values[4], values[3]);
  T.setOrigin(p);
  T.setRotation(q);

  return true;
}

// ----------------------------------------------------------------------------

void Utils::saveSamples(const std::string& file, const std::vector<tf2::Transform>& Ts)
{
  std::ofstream fout(file);

  for (const auto& T : Ts) {
    tf2::Vector3 p = T.getOrigin();
    tf2::Quaternion q = T.getRotation();
    fout << p.x() << "," << p.y() << "," << p.z() << ","
         << q.getW() << "," << q.getX() << "," << q.getY() << "," << q.getZ()
         << std::endl;
  }

  fout.close();
}

} // ns vislam_utils
} // ns acl
