/**
 * @file pose_selector.cpp
 * @brief Allows hot switching of mocap and vislam.
 *        Registers vislam and mocap frames.
 * @author Parker Lusk <parkerclusk@gmail.com>
 * @date 8 July 2019
 */

/**
 *                                  {mocap}
 *                                 /     |
 *                           T_MO /      |
 *                               /       |
 *                           {odom}      |
 *                              |        |
 *                           {grav}      |
 *                              |        | T_MB
 *                         {imu_start}   |
 *                               \       |
 *                          T_IsI \      |
 *                                 \     |
 *                                {imu}  |
 *                                   \   |
 *                               T_IB \  |
 *                                     \ |
 *                                   {body}
 * 
 * The goal is to recover T_MO, the vislam odometry frame w.r.t mocap.
 * This registers the mocap and vislam frames by estimating T_MO, since
 * the vislam frame origin is wherever the drone started from.
 * NOTE: imu (frd); body ~= board (flu), see README of ATLflight/ros-examples
 * 
 */

#include "vislam_utils/pose_selector.h"

namespace acl {
namespace vislam_utils {

PoseSelector::PoseSelector(const ros::NodeHandle nh)
: nh_(nh)
{
  sub_mocap_ = nh_.subscribe("mocap", 1, &PoseSelector::mocapCb, this);
  sub_vislam_ = nh_.subscribe("vislam", 1, &PoseSelector::vislamCb, this);

  pub_pose_ = nh_.advertise<geometry_msgs::PoseStamped>("pose", 1);
  pub_vislam_registered_ = nh_.advertise<geometry_msgs::PoseStamped>("vislam_registered", 1);

  tim_tf_broadcast_ = nh_.createTimer(ros::Duration(0.01), &PoseSelector::timerCb, this);

  srv_sample_ = nh_.advertiseService("sample", &PoseSelector::sampleSrvCb, this);
  srv_select_ = nh_.advertiseService("select", &PoseSelector::selectSrvCb, this);

  nh_.param("nrSamples", nrSamples_, 100);
  nh_.param("shutdown_mocap", shutdown_mocap_, true);

  tf_br_.reset(new tf2_ros::StaticTransformBroadcaster);

  //
  // Vehicle name (from ROS namespace)
  //

  vehname_ = ros::this_node::getNamespace();
  vehname_ = vehname_.substr(vehname_.find_first_not_of('/'));
  std::transform(vehname_.begin(), vehname_.end(), vehname_.begin(),
    [](unsigned char c) { return std::toupper(c); });

  //
  // Initialize pose of vislam odom w.r.t mocap frame
  //

  // header and child frame ids are set in callbacks
  // T_MO_.header.frame_id = "world";
  // T_MO_.child_frame_id = "odom";
  T_MO_.transform.rotation.x = 0.0;
  T_MO_.transform.rotation.y = 0.0;
  T_MO_.transform.rotation.z = 0.0;
  T_MO_.transform.rotation.w = 1.0;
  T_MO_.transform.translation.x = 0.0;
  T_MO_.transform.translation.y = 0.0;
  T_MO_.transform.translation.z = 0.0;

  //
  // body (autopilot) w.r.t imu
  //

  std::string body_wrt_imu;
  nh_.param<std::string>("body_wrt_imu",
                          body_wrt_imu, "0 0 0 0 0 0");

  if (!Utils::parse_xyzYPR(body_wrt_imu, T_IB_)) {
    ROS_ERROR_STREAM("rosparam 'body_wrt_imu' expected "
                      "format 'x y z Y P R' "
                      "but got '" << body_wrt_imu << "'");
    ros::shutdown();
    return;
  }
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

void PoseSelector::mocapCb(const geometry_msgs::PoseStampedPtr& msg)
{
  // body frame w.r.t mocap frame
  tf2::Transform T_MB;
  tf2::convert(msg->pose, T_MB);

  // set the parent (source) frame id
  T_MO_.header.frame_id = msg->header.frame_id;

  // samples for registration / calibration
  if (capture_) buf_T_MB_.push_back(T_MB);

  // relay pose if this stream is selected
  if (!use_vislam_) pub_pose_.publish(msg);
}

// ----------------------------------------------------------------------------

void PoseSelector::vislamCb(const geometry_msgs::PoseStampedPtr& msg)
{
  // set the child (target) frame id using the parent (source) of vislam meas.
  T_MO_.child_frame_id = msg->header.frame_id;

  //
  // Use message to construct body w.r.t odom transform
  //

  tf2::Transform T_OI;
  tf2::fromMsg(msg->pose, T_OI);

  // body frame w.r.t vislam odom frame (x-back, z-down)
  auto T_OB = T_OI * T_IB_;

  //
  // re-publish body w.r.t mocap frame (using estimated T_MO_)
  //

  geometry_msgs::PoseStamped pose;
  pose.header.stamp = msg->header.stamp;
  pose.header.frame_id = T_MO_.header.frame_id;

  tf2::Transform T_MO;
  tf2::convert(T_MO_.transform, T_MO);

  // express {body w.r.t vislam odom} as {body w.r.t mocap}
  auto T_MB = T_MO * T_OB;
  tf2::convert(T_MB, pose.pose);
  pub_vislam_registered_.publish(pose);

  // samples for registration / calibration
  if (capture_) buf_T_OB_.push_back(T_OB);

  // relay registered pose if this stream is selected
  if (use_vislam_) pub_pose_.publish(pose);
}

// ----------------------------------------------------------------------------

bool PoseSelector::sampleSrvCb(std_srvs::Trigger::Request& req,
                               std_srvs::Trigger::Response& res)
{
  // bail if we have already shutdown mocap
  if (use_vislam_ && shutdown_mocap_) {
    res.success = false;
    return true;
  }

  // A capture represents a set of samples that were retrieved, typically from
  // different locations. This helps average out the noise a little more.
  nrCaptures_++;

  // actually, we just reset the calibration each call
  nrCaptures_ = 1;
  buf_T_MB_.clear();
  buf_T_OB_.clear();

  ROS_INFO_STREAM("[Capture " << nrCaptures_ << "] "
                  "Capturing " << nrSamples_ << " transform samples.");

  // begin capturing
  capture_ = true;

  res.success = true;
  return true;
}

// ----------------------------------------------------------------------------

bool PoseSelector::selectSrvCb(std_srvs::SetBool::Request& req,
                               std_srvs::SetBool::Response& res)
{
  // bail if we have already shutdown mocap
  if (use_vislam_ && shutdown_mocap_) {
    res.success = false;
    return true;
  }

  // if we are in the middle of a capture, don't switch
  if (capture_) {
    res.success = false;
    ROS_WARN_STREAM("[PoseSelector] Won't switch---in the middle of registration.");
    return true;
  }

  use_vislam_ = req.data;

  if (use_vislam_) {
    res.message = "Selected VISLAM (" + sub_vislam_.getTopic() + ")";
    ROS_WARN_STREAM("[PoseSelector] Using topic " << sub_vislam_.getTopic());

    if (shutdown_mocap_) {
      // stop listening to mocap
      sub_mocap_.shutdown();

      // nothing left to do
      tim_tf_broadcast_.stop();
    }

  } else {
    res.message = "Selected mocap (" + sub_mocap_.getTopic() + ")";
    ROS_WARN_STREAM("[PoseSelector] Using topic " << sub_mocap_.getTopic());
  }

  res.success = true;
  return true;
}

// ----------------------------------------------------------------------------

void PoseSelector::timerCb(const ros::TimerEvent& e)
{
  // check if we are done capturing
  if (capture_) {
    // How many samples (total) should we expect
    // in the buffer after this capture?
    const int totalSamples = nrCaptures_*nrSamples_;

    if (buf_T_MB_.size() >= totalSamples && buf_T_OB_.size() >= totalSamples) {
      capture_ = false;
      ROS_INFO_STREAM("[Capture " << nrCaptures_ << "] Finished.");

      // make sure that we have the same number of transforms
      buf_T_MB_.resize(totalSamples);
      buf_T_OB_.resize(totalSamples);

      // find the relative transformation between mocap and vislam frames
      calibrate();

      // send latest transform estimate
      T_MO_.header.stamp = ros::Time::now();
      tf_br_->sendTransform(T_MO_);

      // save the samples to file
      Utils::saveSamples("body_wrt_mocap.csv", buf_T_MB_);
      Utils::saveSamples("body_wrt_vislam.csv", buf_T_OB_);
    }
  }
}

// ----------------------------------------------------------------------------

void PoseSelector::calibrate()
{
  ROS_WARN_STREAM("Beginning calibration.");

  // How many total transform samples should we have?
  const size_t totalSamples = nrCaptures_*nrSamples_;
  assert(buf_T_MB_.size() == totalSamples);
  assert(buf_T_OB_.size() == totalSamples);

  //
  // Cumulative moving average filter
  //

  // initialize the average pos, quat
  tf2::Vector3 p_ME;
  tf2::Quaternion q_ME = tf2::Quaternion::getIdentity();

  for (size_t n=0; n<totalSamples; ++n) {
    // measurements of {body wrt mocap} and {body wrt vislam}
    auto Tbar_MB = buf_T_MB_[n];
    auto Tbar_EB = buf_T_OB_[n];

    // new measurement: vislam odom w.r.t mocap frame
    auto Tbar_ME = (Tbar_MB) * (Tbar_EB).inverse();

    // break out rotation (quat) and translation measurement updates
    auto qbar_ME = Tbar_ME.getRotation();
    auto pbar_ME = Tbar_ME.getOrigin();

    // cumulative moving average
    p_ME = 1.0/(n+1) * (n*p_ME + pbar_ME);

    // SLERP for quats (TODO: is this good for larger rotations?)
    q_ME = q_ME.slerp(qbar_ME, 1.0/(n+1));
  }

  // pack as transform
  tf2::Transform T_ME;
  T_ME.setOrigin(p_ME);
  T_ME.setRotation(q_ME);

  // vislam odom frame w.r.t mocap frame (what we are calibrating for)
  tf2::convert(T_ME, T_MO_.transform);

  ROS_WARN_STREAM("Calibration complete.");
}

// ----------------------------------------------------------------------------

} // ns vislam_utils
} // ns acl
