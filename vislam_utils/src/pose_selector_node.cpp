/**
 * @file pose_selector_node.cpp
 * @brief ROS node for hot switching between mocap and vislam
 * @author Parker Lusk <parkerclusk@gmail.com>
 * @date 8 July 2019
 */

#include <ros/ros.h>

#include "vislam_utils/pose_selector.h"

int main(int argc, char *argv[])
{
  ros::init(argc, argv, "pose_selector");
  ros::NodeHandle nh("~");
  acl::vislam_utils::PoseSelector obj(nh);
  ros::spin();
  return 0;
}
