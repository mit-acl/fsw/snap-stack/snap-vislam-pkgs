VOXL VISLAM / VIO / qVIO Packges
================================

Qualcomm developed a visual-inertial odometry package for the original (blue) Snapdragon Flight. It is packaged as part of the [Qualcomm Machine Vision SDK (`mv`)](https://developer.qualcomm.com/software/machine-vision-sdk). This package is included via the VOXL system image and is closed-source. To use VISLAM from the `mv` package, ModalAI has developed extensive support software, namely the [qvio server](https://docs.modalai.com/voxl-qvio-server/). Additionally, there are [introspection tools](https://docs.modalai.com/voxl-inspect-qvio/) that can be used for debugging. All of these packages should already be pre-installed on the VOXL.

The purpose of this repo is to explain how to fly with `qvio` and to provide additional utilities (such as registering the local VIO frame with motion capture).

## Setup

Make sure to have followed instructions in the [voxl-dev](https://gitlab.com/mit-acl/fsw/snap-stack/voxl-dev) repo to setup your VOXL. Ensure you have VOXL Suite version 0.5.0+ installed - see [voxl-dev FAQ](https://gitlab.com/mit-acl/fsw/snap-stack/voxl-dev#voxl-faq) for how to check / upgrade.

The setup consists of two major parts: (1) configuring qvio, imu, and camera servers using the pre-installed ModalAI packages, and (2) building this repo and using it within the `snap-stack` flight stack.

### Configuration

All configuration and commands run on VOXL (not `snapros` docker -- so, ssh into vehicle and make sure the bash prompt is red not yellow).

1. Read https://docs.modalai.com/voxl-qvio-server/
2. Read https://docs.modalai.com/configure-cameras/
3. List services using `voxl-inspect-services`.
4. Enable the camera server with `voxl-configure-cameras`. Unless other cameras are needed, select **2  Tracking only**. Do not rotate.
5. Use `voxl-inspect-services` to see that camera is now enabled. Use `voxl-inspect-cam tracking` to verify that a RAW8 640x480 image is being streamed at 30 Hz with latency of about 10-15 ms.
6. Enable the qvio server with `voxl-configure-qvio`. Usually, not necessary to reset to factory defaults. Use `imu_1_voxl`.
7. List services again and verify that both camera and qvio are enabled and running.
8. Use `voxl-inspect-qvio` to get a status update of the state of qvio algorithm.
9. Read the comments in the file `/etc/modalai/voxl-qvio-server.conf`.
10. Probably need to edit `camera_height_off_ground_m` - see [wiki](https://gitlab.com/mit-acl/fsw/snap-stack/snap-vislam-pkgs/-/wikis/home) for more info.
11. Restart `voxl-qvio-server` with `systemctl restart voxl-qvio-server` and check with `voxl-inspect-services`.
12. Check VIO solution with `voxl-inspect-qvio`. Try moving the VOXL (being cognizant of ideal motions - i.e., smooth). Does it make sense when you come back to the place you started? (Note that the local coordinate frame is `frd`, which is different than `flu` used by snap-stack).

### Using VIO Pose

1. Clone this repo into your catkin workspace on the VOXL.
2. Use `catkin build` (within the `snapros` docker [yellow bash prompt])
3. Expose `voxl-qvio-server` data (and others) to ROS with `roslaunch vislam mpa_to_ros.launch`.
4. Run the pose selector: `roslaunch vislam_utils pose_selector.launch`
5. Relaunch the `snap` autopilot node to subscribe to `pose_selector/pose` instead of directly to mocap: `roslaunch snap snap.launch extpose:=pose_selector/pose`
6. Verify connections in `rqt_graph`.
7. Use `rosservice call /VEH_NAME/pose_selector/sample "{}"` to register vio with mocap (use double tap of TAB and it will auto complete)
8. Use `rosservice call /VEH_NAME/pose_selector/select "data: true"` to swich to vio, use *false* to swithc to mocap.(Steps 7-8 are instructed in [`vislam_utils` README](https://gitlab.com/mit-acl/fsw/snap-stack/snap-vislam-pkgs/-/tree/main/vislam_utils).)

**Note**: I  have had [some problems](https://gitlab.com/voxl-public/ros/voxl_mpa_to_ros/-/issues/4) in the past using `voxl_mpa_to_ros` launched from the `snapros` docker. It is probably best to launch this file from outside the docker (red prompt). Since the workspace is not sourced outside the docker, navigate to the `snap-vislam-pkgs/vislam/launch` directory and call `roslaunch mpa_to_ros.launch` directly.


## Debugging

Number of features is important. See **Resources** for more info. You can also view the `qvio_overlay/image_raw` topic published by the `voxl_mpa_to_ros` package. An example frame is shown below (note - 28 pts is low. The `Q` value gives a quality value, based on the cov det).

![](https://gitlab.com/mit-acl/fsw/snap-stack/snap-vislam-pkgs/-/wikis/assets/qvio_overlay.png)

## Other Resources

- ModalAI forum: https://forum.modalai.com/
- ModalAI's [Troubleshooting VIO](https://docs.modalai.com/flying-with-vio/#troubleshooting-vio)
- API comments for `mv` `VISLAM`: `/usr/include/mv/mvVISLAM.h` (remember source is closed-source, but interface is public)
- Source code of `voxl-qvio-server`, which calls `VISLAM`: https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-qvio-server/-/blob/master/server/main.cpp#L271

## FAQ

- How to see more info / output for debugging?
  - use `systemctl`, e.g., `systemctl status voxl-qvio-server`
- How to force reset a service if it is enabled but not running?
  - use `systemctl`, e.g., `systemctl restart voxl-camera-server`
- How to reset qvio algorithm?
  - use `voxl-reset-qvio`

## Common Errors

- When launching `roslaunch mpa_to_ros.launch` this error comes up. ![Error Related to VOXL Suite Version](./.images/voxl-qvio-package-not-up-to-date-error-when-mpa_to_ros_launch.png)
  - Solution: Update VOXL Suit following [VOXL FAQ](https://gitlab.com/mit-acl/fsw/snap-stack/voxl-dev) 